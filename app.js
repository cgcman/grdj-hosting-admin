var express = require('express');
var http = require('http');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
//var db = require('./models/db');
//var schema = require('./models/schema');
var routes = require('./routes');

var app = express();

var CronJob = require('cron').CronJob;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

app.get('/', routes.index);
app.get('/nuevo', routes.index);
app.get('/login', routes.index);
app.get('/dash', routes.index);
app.get('/dash/:clienteId', routes.index);

var mongoose = require('mongoose');

/////////////////////////////
////// CONEXION LOCAL //////
/////////////////////////////

mongoose.connect('mongodb://localhost/hostingAdmin', function(error){
   if(error){
      throw error; 
   }else{
      console.log('Conectado a MongoDB');
   }
});


/////////////////////////////
////// CONEXION HEROKU //////
/////////////////////////////

/*mongoose.connect('mongodb://' + process.env.MONGOLAB_URI + '/hostingAdmin', function(error){
   if(error){
      throw error; 
   }else{
      console.log('Conectado a MongoDB');
   }
});*/


var ClientesSch = new mongoose.Schema({  
  nombre: String,
  email: String,
  mensual: Number,
  total: Number,
  constancia: { type:String, default:''},
  sitio: String,
  telefono: String,
  telefono_alt: String,
  servicios: String,
  vencimiento: { type: Date, default: Date.now }
});

var Clientes= mongoose.model('Clientes', ClientesSch);

app.get('/clientes', function(req, res){
   Clientes.find({}, function(error, clientes){
      if(error){
         res.send('Error.');
      }else{
         res.send(clientes);
      }
   })
});

app.post('/cliente', function(req, res){
   var cliente = new Clientes({
     nombre: req.query.nombre,
     email: req.query.email,
     mensual: req.query.mensual,
     total: req.query.mensual,
     sitio: req.query.sitio,
     telefono: req.query.telefono,
     telefono_alt: req.query.telefono_alt,
     servicios: req.query.servicios
  });
  cliente.save(function(error, documento){
     if(error){
        res.send('Error.');
     }else{
        res.send(documento);
     }
  });
});

app.put('/cliente/:ID', function(req, res){   
   var id = req.params.ID
   Clientes.findByIdAndUpdate(id, req.body, function(err, documento) {
        if (err) {
            return next(err);
        } else {
            res.json(documento);
        }
        console.log(documento);
    }); 
});

app.delete('/cliente/:ID', function(req, res){
   Clientes.remove({_id: req.params.ID}, function(error, documento){
      if(error){
         res.send('Error.');
      }else{
         res.send(documento);
      }
   }); 
   //console.log(req.params);  
});

/// catch 404 and forwarding to error handler

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers
// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;

/////////////////////////////
////// CONEXION LOCAL //////
/////////////////////////////

app.listen(3000, function(){
    console.log('Escuchando en el puerto 3000');
});

/////////////////////////////
////// CONEXION HEROKU //////
/////////////////////////////

/*app.listen(process.env.PORT || 3000, function(){
    console.log('Escuchando en el puerto Heroku');
});*/



/////////////////////////////
////// CRON JOBS ////////////
/////////////////////////////

var updateIndex=0;
var actuales = new Array();
var nuevos= new Array();

var interval=setInterval(function(){},10000);

function leeData(){
  var cursor = Clientes.find({}, {mensual:1, total:1}, function(error, clientes){
      if(error){
         console.log('error en la consulta');
      }else{
        actuales=clientes;

        actuales.forEach(function(entry) {            
            var nuevo=entry.mensual+entry.total;
            var nuevoObj={'total':nuevo};
            var nuevoArr=[entry._id, nuevoObj];            
            nuevos.push(nuevoArr);            
        });  

        actualizaDB(nuevos[updateIndex][0],nuevos[updateIndex][1]); 
      }
   }) // returns cursor object which is a pointer to result set
}

function actualizaDB(id, data){
  Clientes.findByIdAndUpdate(id, data, function(err, documento) {
        if (err) {
          console.log('error actualizando');
        } else {          
          if((updateIndex+1) == nuevos.length){
            muestraResultado();            
          }else{
            updateIndex++;
            actualizaDB(nuevos[updateIndex][0],nuevos[updateIndex][1]);
          }          
        }
        //console.log(documento);
    });
}

function muestraResultado(){
  var cursor = Clientes.find({}, {mensual:1, total:1}, function(error, clientes){
      if(error){
         console.log('error mostrando');
      }else{
        emailVencimiento();
      }
   })
  updateIndex=0;
}

///En el caso de '10 * * * * *'' , el 10 significa que se ejecuta en el segundo 10 de cada minuto//.

new CronJob('1 1 1 1 * *', function() {
  console.log('se ejecuto el cron en el minuto 10 de cada hora');
  leeData();
}, null, true);


/////////////////////////////
////// EMAIL JOBS ////////////
/////////////////////////////


// create reusable transporter object using SMTP transport 
var transporter = nodemailer.createTransport({
    host: 'mail.grdj.com.ar',
    port: 587,
    tls: {
        rejectUnauthorized: false
    },
    auth: {
        user: 'info@grdj.com.ar',
        pass: 'Grdj4718'
    }
});
 
// NB! No need to recreate the transporter object. You can use 
// the same transporter object for all e-mails 
 
// setup e-mail data with unicode symbols 
var mail_vencimiento = {
    from: 'GRDJ<info@grdj.com.ar>', // sender address 
    to: 'grdj2@grdj2.com.ar', // list of receivers 
    subject: 'Hosting Admin', // Subject line 
    text: 'Se Ejecuto el Vencimiento mensual automático', // plaintext body 
    html: '<b>Se Ejecuto el Vencimiento mensual automático</b>' // html body 
};

//setTimeout(function(){},5000);

function emailVencimiento(){
  transporter.sendMail(mail_vencimiento, function(error, info){
      if(error){
          console.log(error);
      }else{
          console.log('Message sent: ' + info.response);
      }
  });
}