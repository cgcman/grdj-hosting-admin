/*angular.module('app', ['ui.router']).
config(['$urlRouterProvider','$stateProvider', function($urlRouterProvider, $stateProvider) {
	//$locationProvider.html5Mode(true).hashPrefix('!');
	$urlRouterProvider.otherwise('/');

	$stateProvider.
		state('login', {
			url: '/',
			templateUrl: 'templates/login.html',
			controller: 'login'
		})
		.state('dash', {
			url: '/dash',
			templateUrl: 'templates/dashboard.html',
			controller: 'dash',
			resolve:{
				clientes:['$http', function($http){
					return $http.get('models/clientes.json').then(function(response){
						return response.data;
					});					
				}]
			}
		})	
}])*/
var now = new Date();

angular.module('admin', ['ngRoute']).
  config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      $routeProvider
        .when("/login", {
          templateUrl: "templates/login.html",
          controller: "login"})
        .when("/nuevo", {
          templateUrl: "templates/nuevo.html",
          controller: "nuevo"})
        .when('/dash/:clienteId', {
          templateUrl: "templates/dashboard.html",	    
	      controller: 'dash',
	      resolve:{
				clientes:['$http', function($http){
					return $http.get('/clientes').then(function(response){						
						return response.data;
					});					
				}]
			}})        
        .when("/dash", {
          templateUrl: "templates/dashboard.html",
          controller: "dash",
      	  resolve:{
				clientes:['$http', function($http){
					return $http.get('/clientes').then(function(response){												
						return response.data;
					});					
				}]
			}})        
        .otherwise({ redirectTo: "/login"});
    }
  ]
);