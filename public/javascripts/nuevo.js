angular.module('admin').
controller('nuevo', ['$scope','$location','$http', function($scope, $location, $http){
	$scope.titulo='GRDJ - Host Admin';
	$scope.agregar=function(){
		$http({
            method: 'POST',
            url: '/cliente',
            params: {
               nombre: $scope.nombre,
               email: $scope.email,
               mensual: $scope.mensual,
               sitio: $scope.sitio,
               telefono: $scope.telefono,
               telefono_alt: $scope.telefono_alt,
               servicios: $scope.servicios
            }
         }).
         success(function(data) {
         	//console.log(data);
            if(typeof(data) == 'object'){
               $location.path('/dash'); 
            }else{
               alert('Error al intentar guardar el Evento.');
            }
         }).
         error(function() {
            alert('Error al intentar guardar el Evento.');
         });
	}
}])