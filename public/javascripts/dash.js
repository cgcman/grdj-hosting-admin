angular.module('admin').
controller('dash', ['$scope', 'clientes','$filter','$routeParams','$http', '$location', function($scope, clientes,$filter, $routeParams, $http, $location){  
	$scope.isVisible=false;
  $scope.titulo='Listado de clientes';
  $scope.fechaActual=now;

  var newClientes=clientes;  

  for(var a=0; a< clientes.length; a++){
      var then = new Date(clientes[a].vencimiento);
      var dia = new Date(newClientes[a].vencimiento).getDate();
      var mes = (new Date(newClientes[a].vencimiento).getMonth());
      var ano = (new Date(newClientes[a].vencimiento).getFullYear())+1;
      
      newClientes[a].venceStr=dia+' / '+(mes+1)+' / '+ano;

      newClientes[a].vence = {
         value: new Date(ano, mes, dia)
      };

      if(now > then){
        newClientes[a].vencido=true;
      }else{
        newClientes[a].vencido=false;
      }                 
  }

  $scope.clientes=newClientes;  
  //console.log($scope.clientes);  


	if($routeParams.clienteId==undefined){
		console.log('muestra todos');
	}else{		
		var cliarr = $filter('filter')($scope.clientes,{_id:$routeParams.clienteId})	
      $scope.cliente=cliarr[0];

      /*var then = new Date(cliarr[0].vencimiento);

      if(now > then){
        console.log('vencio');
      }else{
        console.log('no vencio');
      }*/      	
	}

  $scope.actualizar=function(){
      $scope.isVisible=true;     
  }

	$scope.guardar=function(ID){
    $scope.cliente.vencimiento=$scope.cliente.vence.value.toISOString();
    console.log($scope.cliente.vencimiento);
		$http.put('/cliente/'+ID, $scope.cliente)
        .success(function(data) {                        
            if(typeof(data) == 'object'){
               $scope.isVisible=false;
               $location.path('/dash/'+$routeParams.clienteId); 
            }else{
               alert('Error al intentar guardar el Evento.');
            }
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });            
	}

	$scope.eliminar=function(ID){
		$http({
            method: 'DELETE',
            url: '/cliente/'+ID            
         }).
         success(function(data) {
         	//console.log(data);
            if(typeof(data) == 'object'){
               $location.path('/dash/'); 
            }else{
               alert('Error al intentar guardar el Evento.');
            }
         }).
         error(function() {
            alert('Error al intentar guardar el Evento.');
         });
	}
	/*$scope.filtraCliente = function (ID) {
	    $scope.cliente = $filter('filter')($scope.clientes,{id:ID})
	};*/		
}])