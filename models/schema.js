var mongoose = require('mongoose');
var ClientesSch = new mongoose.Schema({  
  nombre: String,
  email: String,
  mensual: Number,
  total: Number,
  constancia: { type:String, default:''},
  sitio: String,
  telefono: String,
  telefono_alt: String,
  servicios: String,
  vencimiento: { type: Date, default: Date.now }
});
mongoose.model('Clientes', ClientesSch);