angular.module('app', ['ui.router']).
config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider, $stateProvider) {
	$urlRouterProvider.otherwise('/');

	$stateProvider.
		state('login', {
			url: '/',
			templateUrl: 'templates/login.html',
			controller: 'login'
		})
		.state('dash', {
			url: '/dash',
			templateUrl: 'templates/dashboard.html',
			controller: 'dash',
			resolve:{
				clientes:['$http', function($http){
					return $http.get('models/clientes.json').then(function(response){
						return response.data;
					});					
				}]
			}
		})	
}])
