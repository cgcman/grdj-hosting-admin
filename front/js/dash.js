angular.module('app').
controller('dash', ['$scope', 'clientes','$filter', function($scope, clientes,$filter){
	$scope.titulo='Listado de clientes';
	$scope.clientes=clientes.clientes;
	$scope.cliente={"nombre":"Claudio"};

	$scope.filtraCliente = function (ID) {
	    $scope.cliente = $filter('filter')($scope.clientes,{id:ID})
	};
}])
