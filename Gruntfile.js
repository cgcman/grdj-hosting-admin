var path = require('path');



// Stylus configuration
var stylusLinenos = false;
var stylusCompress = true;
// we should defenitely use this, but for some reason it's working right now
// http://blog.sapegin.me/all/css-workflow
// var stylusAutoprefixer = require('autoprefixer-stylus')({
//   browsers: ['Chrome >= 20']
// });

/* Testing utility. To use in styl files, simply write:
 * consoleLog("SOMETHING TO LOG")
 */
function stylusConsole(node) {
  console.log('%s  %s', node.val, node.type);
}

// workaround for avoiding forking bootstrap-styl and doing the following:
// https://github.com/shomeya/bootstrap-stylus/blob/v1.4.0-stylus/lib/index.js
var resolve = require('resolve');
var stylusPath;
function stylusBootstrap3lLoader() {
  return function(style) {
    if (!stylusPath) {
      stylusPath = resolve.sync('bootstrap-styl/bower', {
        extensions: ['.json']
      });
      stylusPath = path.dirname(stylusPath) + '/bootstrap'
    }
    style.include(stylusPath);
  };
}


module.exports = function(grunt) {
  var env = process.env.NODE_ENV;

  grunt.initConfig({
    pkg: this.file.readJSON('package.json'),

    stylus: {
      // Bootstrap 3/responsive stylesheets
      dist: {
        options: {
          use: [ stylusBootstrap3lLoader/*, stylusAutoprefixer*/ ],
          define: {
            consoleLog: stylusConsole
          },
          linenos: stylusLinenos,
          compress: stylusCompress
        },
        files: [{
          expand: true,
          cwd   : 'stylus',
          src   : [
            'global.styl'
          ],
          dest  : 'public/stylesheets',
          ext   : '.css'
        }]
      }
    },

    autoprefixer: {
      options: {
        browsers: [
          'Android 2.3',
          'Android >= 4',
          'Chrome >= 20',
          'Firefox >= 24', // Firefox 24 is the latest ESR
          'Explorer >= 8',
          'iOS >= 6',
          'Opera >= 12',
          'Safari >= 6'
        ],
        map: false
      },
      // check every stylesheet for vendor prefixes
      dist: {
        src: [
          'public/stylesheets/*.css'
        ]
      }
    }

  });


  // Set up tasks
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-autoprefixer');

  grunt.registerTask('styles', ['stylus', 'autoprefixer']);

};
